package com.ampex.adapter;

import com.ampex.amperabase.IKiAPI;
import engine.IByteCodeEngine;
import mining_pool.IPool;

public interface KiAdapter extends IKiAPI {

    IPool getPoolManager();
    IByteCodeEngine getBCE8();
}
